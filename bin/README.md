# Known Issues of Using PPT Text Detector Binary

Because of the complexities involving in Theano's CPU/GPU, and cuda-related settings and the limitation of the used **PyInstaller**, it is impossible for us to provide a single binary that is runable under different system settings. Finally, we decide to support the most common use case, namely, decoding using CPUs.

Therefore, if your machine has GPUs or cuda installed, you are expected to meet error messages like follows.
```
mod.cu:3:20: fatal error: Python.h: No such file or directory
ERROR (theano.sandbox.cuda): Failed to compile cuda_ndarray.cu
OSError: [Errno 2] No such file or directory: '..../ppt_text_detector/theano/sandbox/cuda/cuda_ndarray.cu'
```
Because the theano code automatically detemines its compling mode according to your local system, it **imports** a complete **new** set of python dependencies when GPU/cuda is available, but this is not included in the pre-complied binary whose hostmachine only has CPU. Therefore, this import will fail and raise errors. 

Below is a workable solution that we tested on GPU machines. In short, you need to remove `cuda`-related things in your `$PATH` and `$LD_LIBRARY_PATH`, namely,
```
export LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | perl -F':' -lane 'print join("\n",@F)' | grep -v cuda | perl -ne 'BEGIN{@a=();}chomp; push(@a,$_); END{print join(":",@a) . "\n"}'`
export PATH=`echo $PATH | perl -F':' -lane 'print join("\n",@F)' | grep -v cuda | perl -ne 'BEGIN{@a=();}chomp; push(@a,$_); END{print join(":",@a) . "\n"}'`
```
Also, you need to reset the theano's config file, e.g.
```
cp bin/.theanorc ~/.theanorc
```
Please pay attention to the blas lib in `theanorc`. Modify it to point to your `atlas` lib.

Hopefully, these efforts could solve your problem.





